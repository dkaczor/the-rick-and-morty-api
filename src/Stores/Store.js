import appReducer from '../Reducers/AppState';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

const store = createStore(
    appReducer,
    applyMiddleware(thunk)
);

export default store;
