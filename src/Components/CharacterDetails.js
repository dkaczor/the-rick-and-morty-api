import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, Row } from 'react-bootstrap';
import { getCharacterEpisodes } from '../Actions/REST.js';
import { connect } from 'react-redux';
import CharacterDetailsCard from './CharacterDetailsCard';
import Page404 from './WrongPage.js';
import { URLS } from '../Constants/StateConstants';

class CharacterDetails extends Component {
    constructor() {
        super();
        this.state = { 'error': false }
    }

    componentDidMount() {
        this.props.dispatch({ 'type': `RESET_MOVIES` })
        fetch(URLS.list + this.props.match.params.id)
            .then((data) => data.json())
            .then((data) => {
                if (data.hasOwnProperty(`error`) === false) {
                    this.setState({ data });
                } else {
                    this.setState({ 'error': true });
                }
            });
    }

    prepareEpisodeData() {
        if (this.state.data !== undefined) {
            this.props.dispatch(getCharacterEpisodes(this.state.data.episode));
        }
    }

    prepareList(movies) {
        let episodes = <tr></tr>;

        if (movies.length === 0) {
            this.prepareEpisodeData();
        } else {
            episodes = movies.map((item) => <tr key ={item.name}>
                <td>
                    {item.name}
                </td>
                <td>
                    {item.air_date}
                </td>
            </tr>);
        }

        return episodes;
    }

    render() {
        const { movies } = this.props,
            episodes = this.prepareList(movies);

        return (
            <Container>
                <Row>
                    {this.state.error && <Page404 text="Wrong char ID" />}
                    {this.state.data !== undefined &&
                        <CharacterDetailsCard episodes = {episodes} data = {this.state.data} />
                    }
                </Row>
            </Container>
        )
    }
}

const mapStateToProps = (state) => ({ 'movies': state.movies });

CharacterDetails.propTypes = {
    'dispatch': PropTypes.func.isRequired,
    'history': PropTypes.object.isRequired,
    'match': PropTypes.object.isRequired,
    'movies': PropTypes.array
};

export default connect(mapStateToProps)(CharacterDetails);
