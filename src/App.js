import React from 'react';
import CharactersList from './Components/CharactersList';
import CharacterDetails from './Components/CharacterDetails';
import Page404 from './Components/WrongPage';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={CharactersList} />
        <Route path="/character/:id" component = {CharacterDetails}  />
        <Route component={() => <Page404 text="Page does not exist!" />} />
      </Switch>
    </Router>
  )
}

export default App;
