# Rick and Morty API client

## Technologies
Project is created with:
* Npm: 5.60
* React: 16.8.6
* Node.js: 8.10
UI has been created with React-Bootstrap

## Run project
To run this project, clone this repository, then use commands below, using npm:
```
$ cd the-rick-and-morty-api
$ npm install
$ npm start
```
