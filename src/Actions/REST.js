import * as CONSTANTS from '../Constants/StateConstants';

async function fetchPage(page) {
    try {
        return await fetch(page)
            .then((data) => data.json())
            .then((data) => data);
    } catch (error) {
        return error;
    }
}

function handleGetData(type, dispatch, data, appData) {
    let ret = null;

    switch (type) {
    case CONSTANTS.INITIAL_FETCH:
        dispatch({
            'data': data.results,
            'max': data.info.pages,
            type
        });
        ret = data;
        break;

    case CONSTANTS.FETCHED_LOCAL_STORAGE:
        dispatch({
            'data': JSON.parse(localStorage.data),
            'max': parseInt(localStorage.max, 10),
            'page': parseInt(localStorage.page, 10),
            type
        });
        break;

    case CONSTANTS.FETCHED_MORE_DATA:
        dispatch({
            'data': data.results,
            type
        });
        ret = data;
        break;

    case CONSTANTS.FETCHED_ALL_DATA:
        ret = appData;
        for (const index of data) {
            dispatch({
                'data': index.results,
                'type': CONSTANTS.FETCHED_MORE_DATA
            });
            ret = [
                ...ret,
                ...index.results
            ];

        }
        break;

    case CONSTANTS.MOVIES:
    case CONSTANTS.MOVIES_ERROR:
        dispatch({
            data,
            type
        });
        break;
    default:
        break;

    }

    return ret;
}

function fetchCharacterList() {
    return (dispatch) => {
        if (localStorage.data === undefined) {
            fetchPage(CONSTANTS.URLS.list)
                .then((data) => handleGetData(CONSTANTS.INITIAL_FETCH, dispatch, data))
                .then((data) => {
                    localStorage.setItem(`data`, JSON.stringify(data.results));
                    localStorage.setItem(`max`, data.info.pages);
                    localStorage.setItem(`page`, 1);
                })
        } else {
            handleGetData(CONSTANTS.FETCHED_LOCAL_STORAGE, dispatch);
        }
    }
}

function getMoreCharacters(appData, page) {
    return (dispatch) => fetchPage(CONSTANTS.URLS.char + page)
        .then((data) => handleGetData(CONSTANTS.FETCHED_MORE_DATA, dispatch, data))
        .then((data) => {
            localStorage.setItem(`data`, JSON.stringify([
                ...appData,
                ...data.results
            ]));
            localStorage.setItem(`page`, page);
        });
}

function fetchPages(currentPage, max) {
    const items = [];
    let page = currentPage;

    while (page <= max) {
        items.push(fetchPage(CONSTANTS.URLS.char + page));
        page += 1;
    }

    return {
        items,
        "props": {
            page
        }
    };
}

function getAllCharacters(appData, currentPage, max) {
    const results = fetchPages(currentPage, max);

    return (dispatch) => {
        Promise.all(results.items)
            .then((data) => handleGetData(CONSTANTS.FETCHED_ALL_DATA, dispatch, data, appData))
            .then((data) => {
                localStorage.setItem(`data`, JSON.stringify(data));
                localStorage.setItem(`page`, results.page);
            })
            .catch((err) => {
                dispatch({
                    'data': err,
                    'type': CONSTANTS.MOVIES_ERROR
                });
            });
    }
}

function getCharacterEpisodes(links) {
    const results = [];

    return (dispatch) => {
        for (const link of links) {
            results.push(fetchPage(link));
        }
        Promise.all(results)
            .then((data) => handleGetData(CONSTANTS.MOVIES, dispatch, data))
            .catch((err) => handleGetData(CONSTANTS.MOVIES_ERROR, dispatch, err));
    }
}

export { getMoreCharacters, fetchCharacterList, getCharacterEpisodes, getAllCharacters };
