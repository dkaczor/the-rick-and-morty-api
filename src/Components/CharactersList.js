import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { fetchCharacterList} from "../Actions/REST.js";
import { connect } from 'react-redux';
import { Row, Col, Container } from 'react-bootstrap';
import CharacterCard from './CharacterCard.js';
import LoadingButtons from './LoadingButtons.js';

class CharactersList extends Component {
    
    componentDidMount() {
        this.props.dispatch(fetchCharacterList());
    }

    prepareData(data) {
        return data.map((item) =>
            <Col key = {item.id} lg = {3} md={4} sm={6} xs={12}>
                <CharacterCard data = {item} />
            </Col>)
    }

    render() {
        const { data } = this.props,
            showData = this.prepareData(data);

        return (
            <div>
                <Container>
                    <Row className = "buttons-group">
                        <LoadingButtons />
                    </Row>
                    <Row>
                        {showData}
                    </Row>
                </Container>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({ 'data': state.data,
    'max': state.max,
    'page': state.page });

CharactersList.propTypes = {
    'data': PropTypes.array,
    'dispatch': PropTypes.func.isRequired,
    'max': PropTypes.number,
    'page': PropTypes.number
};

export default connect(mapStateToProps)(CharactersList);
