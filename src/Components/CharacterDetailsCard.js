import React from 'react';
import PropTypes from 'prop-types';
import { Card, Row, Col, Table, Button } from 'react-bootstrap';
import './CharacterCard.css';
import { withRouter } from 'react-router-dom';

function CharacterDetailsCard(props) {
    return (
        <Card>
            <Card.Header as = "h2" className = "text-center">
                {props.data.name}
            </Card.Header>
            <Card.Body>
                <Row>
                    <Col md={5} sm ={6} xs ={12}>
                        <Card.Text>
                            <img className = "details-img"
                                src = {props.data.image}
                                alt = {props.data.name} />
                        </Card.Text>
                    </Col>
                    <Col md={7} sm ={12} xs ={12}>
                        <Card.Text>
                            <span>
                                <strong>Status: </strong>
                                {props.data.status}
                            </span>
                            <span>
                                <strong>Species: </strong>
                                {props.data.species}
                            </span>
                            <span>
                                <strong>Type: </strong>
                                {props.data.type}
                            </span>
                            <span>
                                <strong>Gender: </strong>
                                {props.data.gender}
                            </span>
                            <span>
                                <strong>Origin name: </strong>
                                {props.data.origin.name}
                            </span>
                            <span>
                                <strong>Location name: </strong>
                                {props.data.location.name}
                            </span>
                            <span>
                                <strong>Date Created: </strong>
                                {props.data.created.substring(0, 10)}
                            </span>
                        </Card.Text>
                    </Col>
                </Row>
                <Row>
                    <Col md={12} sm ={12} xs ={12}>
                        <Table>
                            <thead>
                                <tr>
                                    <th>Episode name </th>
                                    <th>Air Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                {props.episodes}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
                <Row>
                    <Button className = "back-button" variant = "primary"
                        onClick={props.history.goBack}>
                        Back
                    </Button>
                </Row>
            </Card.Body>
        </Card>
    )
}

CharacterDetailsCard.propTypes = {
    'data': PropTypes.object,
    'episodes': PropTypes.oneOfType([
        PropTypes.array,
        PropTypes.object
    ]),
    'history': PropTypes.object.isRequired,
    'match': PropTypes.object.isRequired
};

export default withRouter(CharacterDetailsCard);
