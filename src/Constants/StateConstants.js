const FETCHED_ALL_DATA = `FETCHED_ALL_DATA`,
      FETCHED_LOCAL_STORAGE = `FETCHED_LOCAL_STORAGE`,
      FETCHED_MORE_DATA = `FETCHED_MORE_DATA`,
      INITIAL_FETCH = `INITIAL_FETCH`,
      MOVIES = `MOVIES`,
      MOVIES_ERROR = `MOVIES_ERROR`,
      RESET_MOVIES = `RESET_MOVIES`,
      URLS = {
        'char': `https://rickandmortyapi.com/api/character/?page=`,
        'list': `https://rickandmortyapi.com/api/character/`
      };

export { FETCHED_ALL_DATA, INITIAL_FETCH, FETCHED_MORE_DATA,
        FETCHED_LOCAL_STORAGE, MOVIES,
        RESET_MOVIES, MOVIES_ERROR, URLS };
