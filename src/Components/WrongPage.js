import React from 'react';
import PropTypes from 'prop-types';
import './Wrong.css';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button';

function Page404(props) {
    return (
        <div className="Wrong">
            <p>
                {props.text}
                <br />
                <Button variant="primary">
                    <Link to={`/`}>
                    Back to mainpage
                    </Link>
                </Button>
            </p>
        </div>
    )
}

Page404.propTypes = {
    'text': PropTypes.string
};

export default Page404;
