import React from 'react';
import PropTypes from 'prop-types';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import './CharacterCard.css';
import { Link } from 'react-router-dom';

function CharacterCard(props) {
    return (
        <Card style = {{ 'width': `100%` }}>
            <Card.Img variant="top" src={props.data.image} />
            <Card.Body>
                <Card.Title>
                    <strong>{props.data.name}</strong>
                </Card.Title>
                <hr />
                <Card.Text>
                    <span>
                        <strong>Status: </strong>
                        {props.data.status}
                    </span>
                    <span>
                        <strong>Species: </strong>
                        {props.data.species}
                    </span>
                </Card.Text>
                <Button variant = "primary">
                    <Link to={`/character/${props.data.id}`}>
                            More details
                    </Link>
                </Button>
            </Card.Body>
            <Card.Footer>
            </Card.Footer>
        </Card>
    )
}

CharacterCard.propTypes = {
    'data': PropTypes.object
};

export default CharacterCard;
