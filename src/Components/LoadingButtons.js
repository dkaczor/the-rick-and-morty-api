import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getMoreCharacters, getAllCharacters } from '../Actions/REST.js';
import { connect } from 'react-redux';
import { Button } from 'react-bootstrap';

class LoadingButtons extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.loadAll = this.loadAll.bind(this);
        this.state = { 'buttonDisabled': false };
    }

    loadAll(data, page, max) {
        this.setState({ 'buttonDisabled': true });
        this.props.dispatch(getAllCharacters(data, page, max));
    }

    handleClick(data, page, max) {
        if (page <= max) {
            this.setState({ 'buttonDisabled': true })
            this.props.dispatch(getMoreCharacters(data, page))
                .then(() => this.setState({ 'buttonDisabled': false }));
        }
    }

    render() {
        const { data, page, max } = this.props;
        return (
                    <>
                        {page < max
                            ? <>
                                <Button disabled = {this.state.buttonDisabled} variant = "success"
                                    onClick = { () => this.loadAll(data, page + 1, max)}>
                                        ALL
                                </Button>
                                <br />
                                <Button disabled = {this.state.buttonDisabled}
                                    onClick = {() => this.handleClick(data, page + 1, max)}>
                                        MORE
                                </Button>
                            </>
                            : ``
                        }
                    </>
        )
    }
}

const mapStateToProps = (state) => ({ 'data': state.data,
    'max': state.max,
    'page': state.page });

LoadingButtons.propTypes = {
    'data': PropTypes.array,
    'dispatch': PropTypes.func.isRequired,
    'max': PropTypes.number,
    'page': PropTypes.number
};

export default connect(mapStateToProps)(LoadingButtons);
