import * as CONSTANTS from '../Constants/StateConstants';

const appInitialState = { 'appState': `INITIAL`,
        'data': [],
        'max': null,
        'movies': [],
        'page': 0 },

    appReducer = (state = appInitialState, action) => {
        switch (action.type) {
        case CONSTANTS.INITIAL_FETCH:
            return { ...state,
                'appState': CONSTANTS.INITIAL_FETCH,
                'data': action.data,
                'max': action.max,
                'page': state.page + 1 };
        case CONSTANTS.FETCHED_MORE_DATA:
            return { ...state,
                'appState': CONSTANTS.FETCHED_MORE_DATA,
                'data': [
                    ...state.data,
                    ...action.data
                ],
                'page': state.page + 1 };
        case CONSTANTS.FETCHED_LOCAL_STORAGE:
            return { ...state,
                'appState': CONSTANTS.FETCHED_LOCAL_STORAGE,
                'data': action.data,
                'max': action.max,
                'page': action.page };
        case CONSTANTS.MOVIES:
            return { ...state,
                'movies': action.data };
        case CONSTANTS.RESET_MOVIES:
            return { ...state,
                'movies': [] };
        case CONSTANTS.MOVIES_ERROR:
            return { ...state,
                'movies': CONSTANTS.MOVIES_ERROR };
        default:
            return { ...state };
        }
    };

export default appReducer;
